#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <pthread.h>
#include <mcheck.h>
#define ERROR_CREATE_THREAD -11
#define ERROR_JOIN_THREAD   -12
#define BAD_MESSAGE         -13
#define SUCCESS               0
//структура узел
typedef struct _Node{
   void *value;
  struct _Node *next;
 struct _Node *prev;
} Node;
//структура двусвязный список будет хранить свой размер
typedef struct _DblLinkedList{
    size_t size;
    Node *head;
    Node *tail;
}DblLinkedList;

DblLinkedList* createDblLinkedList(){  //функция создаёт экземпляр структуры
    DblLinkedList *tmp=(DblLinkedList*)malloc(sizeof(DblLinkedList));
   tmp->size=0;
  tmp->head=tmp->tail=NULL;
 return tmp; 
}
//функция удаления списка
void deleteDblLinkedList(DblLinkedList **list){
    Node *tmp=(*list)->head;
    Node *next=NULL;
    while (tmp){
        next=tmp->next;
        free(tmp);
        tmp=next;
    }
    free(*list);
    (*list)=NULL;
}
//функция вставки спереди
void pushFront(DblLinkedList *list,void *data){
    Node *tmp=(Node*)malloc(sizeof(Node));
    if(tmp==NULL){
        exit(1);//may be failure!!!!!!!!
    }
    tmp->value=data;
    tmp->next=list->head;
    tmp->prev=NULL;
    if(list->head){
       list->head->prev=tmp;
    }
    list->head=tmp;
    if (list->tail==NULL){
        list->tail=tmp;
    }
    list->size++;
}
//удаление из начала списка
void* popFront(DblLinkedList *list) {
    Node *prev;
    void *tmp;
    if (list->head == NULL) {
        exit(2);
    }
 
    prev = list->head;
    list->head = list->head->next;
    if (list->head) {
        list->head->prev = NULL;
    }
    if (prev == list->tail) {
        list->tail = NULL;
    }
    tmp = prev->value;
    free(prev);
 
    list->size--;
    return tmp;
}


//вставка в конец списка
void pushBack(DblLinkedList *list, void *value) {
    Node *tmp = (Node*) malloc(sizeof(Node));
    if (tmp == NULL) {
        exit(3);
    }
    tmp->value = value;
    tmp->next = NULL;
    tmp->prev = list->tail;
    if (list->tail) {
        list->tail->next = tmp;
    }
    list->tail = tmp;
 
    if (list->head == NULL) {
        list->head = tmp;
    }
    list->size++;
}
//удаление с конца
void* popBack(DblLinkedList *list) {
    Node *next;
    void *tmp;
    if (list->tail == NULL) {
        exit(4);
    }
 
    next = list->tail;
    list->tail = list->tail->prev;
    if (list->tail) {
        list->tail->next = NULL;
    }
    if (next == list->head) {
        list->head = NULL;
    }
    tmp = next->value;
    free(next);
 
    list->size--;
    return tmp;
}
//получение н-го элемента
Node* getNthq(DblLinkedList *list, size_t index) {
    Node *tmp = NULL;
    size_t i;
     
    if (index < list->size/2) {
        i = 0;
        tmp = list->head;
        while (tmp && i < index) {
            tmp = tmp->next;
            i++;
        }
    } else {
        i = list->size - 1;
        tmp = list->tail;
        while (tmp && i > index) {
            tmp = tmp->prev;
            i--;
        }
    }
 
    return tmp;
}
//функция вставки узла
void insert(DblLinkedList *list, size_t index, void *value) {
    Node *elm = NULL;
    Node *ins = NULL;
    elm = getNthq(list, index);//attention!!!!!!!!!
    if (elm == NULL) {
        exit(5);
    }
    ins = (Node*) malloc(sizeof(Node));
    ins->value = value;
    ins->prev = elm;
    ins->next = elm->next;
    if (elm->next) {
        elm->next->prev = ins;
    }
    elm->next = ins;
 
    if (!elm->prev) {
        list->head = elm;
    }
    if (!elm->next) {
        list->tail = elm;
    }
 
    list->size++;
}
//функция удаления узла
void* deleteNth(DblLinkedList *list, size_t index) {
    Node *elm = NULL;
    void *tmp = NULL;
    elm = getNthq(list, index);//attention!!!!!!!!!!
    if (elm == NULL) {
        exit(5);
    }
    if (elm->prev) {
        elm->prev->next = elm->next;
    }
    if (elm->next) {
        elm->next->prev = elm->prev;
    }
    tmp = elm->value;
 
    if (!elm->prev) {
        list->head = elm->next;
    }
    if (!elm->next) {
        list->tail = elm->prev;
    }
 
    free(elm);
 
    list->size--;
 
    return tmp;
}
//печать всего списка
void printDblLinkedList(DblLinkedList *list, void (*fun)(void*)){
    Node *tmp=list->head;
    while (tmp){
        fun(tmp->value);
        tmp=tmp->next;
    }
    printf("\n ");
}
/*
void printValue(void *value){
    printf("%d",*((int*)value));
}*/
int Getsize(DblLinkedList *list ){
    return list->size;}
// подсчёт с головы
void* thread_tail(void* thread_data){
    int k=0;
       for(int i=0;i<Getsize(thread_data);i++){
           if(*((int*)(getNthq(thread_data,i))->value)==1)
                   {
                    k++;
                    printf(" %d" ,*((int*)(getNthq(thread_data,i))->value));
                    popBack(thread_data);
                   }
       }
       printf("%d",k);
       return SUCCESS;
}
void* thread_head(void* thread_data){
    int n=0;
    for (int i=Getsize(thread_data);i>0;i--){
        if(*((int*)(getNthq(thread_data,i))->value)==0)
        {
            n++;
            popFront(thread_data);
        }
    }
    printf("%d",n);
    return SUCCESS;
}
int  main(int argc,char* argv[]){
   srand(time(NULL));
   DblLinkedList *list = createDblLinkedList();
   int a=0;
   int n=0;
   int status_addr;
   int status=0;
   scanf("%i",&n);
   pthread_t* threads=(pthread_t*)malloc(n*sizeof(pthread_t));
   for (int i=0;i<n;i++){
        a=rand()%2;
        pushFront(list,&a);

     }
       pthread_create(&(threads[0]),NULL,thread_head,&list);
       pthread_create(&(threads[1]),NULL,thread_tail,&list);
   //    pthread_join(threads[i], NULL);
   
   for(int i = 0; i <= 1; i++){
		pthread_join(threads[i], (void**)&status_addr);
        if (status != SUCCESS) {
            printf("main error: can't join thread, status = %d\n", status);
            exit(ERROR_JOIN_THREAD);
        }
        printf("joined with address %d\n", status_addr);
  }
  for (int i=Getsize(list);i>0;i--){
      printf("%d", *((int*)(getNthq(list, i))->value));
  }
  free(threads);
    deleteDblLinkedList(&list);
}

